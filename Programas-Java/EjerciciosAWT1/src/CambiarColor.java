/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alumno
 */
import java.awt.*;
public class CambiarColor extends java.applet.Applet {

      Choice ColorFondo;
      Button Restablecer;
      
    /**
     * Initializes the applet CambiarColor
     */
    @Override
    public void init() {

      setLayout(null);
      resize(400,200);
      
      ColorFondo=new Choice();
      ColorFondo.setBounds(10, 10, 150, 25);
      
      ColorFondo.add("Color Fondo");
      ColorFondo.add("Fondo Rojo");
      ColorFondo.add("Fondo Verde");
      ColorFondo.add("Fondo Amarillo");
      ColorFondo.add("Fondo Azul");
      
      add(ColorFondo);
      
      Restablecer=new Button("Restablecer");
      Restablecer.setBounds(170, 10, 100, 25);
      
      add(Restablecer);
    }
    
    @Override
    public boolean action(Event e,Object o)
    {
        int color=ColorFondo.getSelectedIndex();
        switch (color)
        {
            case 1: setBackground(Color.RED);break;
            case 2: setBackground(Color.GREEN);break;
            case 3: setBackground(Color.YELLOW);break;
            case 4: setBackground(Color.BLUE);break;
        }
        
        showStatus(ColorFondo.getSelectedItem());
        
        if(e.target==Restablecer)
        {
            setBackground(Color.WHITE);
            showStatus("Empezando el programa...");
            ColorFondo.select(0);
        }
        return true;
    }
        
        
        
    

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
