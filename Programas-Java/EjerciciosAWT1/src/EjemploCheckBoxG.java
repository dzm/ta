/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * EjemploCheckBoxG.java
 *
 * Created on 24-oct-2012, 17:37:51
 */
/**
 *
 * @author Alumno
 */
import java.awt.*;
public class EjemploCheckBoxG extends java.applet.Applet {

            Checkbox chkMusica,chkDeporte,chkAventura;
            CheckboxGroup bgrHobby;
            TextArea txts;
    /** Initializes the applet EjemploCheckBoxG */
    @Override
    public void init() {
        setLayout(null);
        
        bgrHobby=new CheckboxGroup();
        
        chkMusica=new Checkbox("Musica",bgrHobby,false);
        chkMusica.setBounds(10,10,100,20);
        add(chkMusica);
        
        chkDeporte=new Checkbox("Deporte",bgrHobby,false);
        chkDeporte.setBounds(10,45,100,20);
        add(chkDeporte);
        
        chkAventura=new Checkbox("Aventura",bgrHobby,false);
        chkAventura.setBounds(10,80,100,20);
        add(chkAventura);
        
        txts=new TextArea();
        txts.setBounds(120,10,300,200);
        add(txts);
    }
    
    @Override
    public boolean action(Event e,Object o)
    {
     String aux="Ud. eligio: ";
     if(chkMusica.getState()==true)
         aux+=" Musica, ";
     if(chkDeporte.getState()==true)
         aux+=" Deporte, ";
     if(chkAventura.getState()==true)
         aux+=" Aventura, ";
     txts.setText(aux);
     return true;
    }
        
        
/** This method is called from within the init() method to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
