/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author David
 */
import java.awt.*;
public class Ejercicio2AWT extends java.applet.Applet {

    private Label nom,sex,sop,pasa,curso;
    private TextField nomb;
    Checkbox Mas, Fem, Musica, Deporte, Aventura;
    CheckboxGroup Sexo;
    private Button mostrar;
    TextArea txt;
    private Choice siso;
    private List lista;
   
    @Override
    public void init() {
        
        setLayout(null);
        resize(600,400);
        
        nom = new Label("Nombre");
        nom.setBounds(10,10,100,25);
        nomb = new TextField(20);
        nomb.setBounds(150,10,100,25);
        
        Sexo=new CheckboxGroup();
        sex = new Label("Sexo");
        sex.setBounds(10,40,100,25);
        Mas = new Checkbox("Masculino",Sexo,false);
        Mas.setBounds(150,40,100,25);
        Fem = new Checkbox("Femenino",Sexo,false);
        Fem.setBounds(150,70,100,25);
        
        sop = new Label("Sistema Operativo");
        sop.setBounds(10,100,100,25);
        siso = new Choice();
        siso.addItem("Windows");
        siso.addItem("Linux");
        siso.setBounds(150,100,100,25);
        
        mostrar = new Button("Mostrar");
        mostrar.setBounds(300,100,100,25);
        
        
        pasa = new Label("Pasatiempo");
        pasa.setBounds(10,130,100,25);
        Musica = new Checkbox("Musica");
        Musica.setBounds(150,130,100,25);
        Deporte = new Checkbox("Deporte");
        Deporte.setBounds(150,160,100,25);
        Aventura = new Checkbox("Aventura");
        Aventura.setBounds(150,190,100,25);

        
        curso = new Label("Curso");
        curso.setBounds(10,230,100,25);
        lista = new List(4,true);
        lista.addItem("PHP");
        lista.addItem("JAVA");
        lista.addItem("ASP.NET");
        lista.addItem("ASP");
        lista.setBounds(150,230,120,100);
        
        txt = new TextArea();
        txt.setBounds(450,10,200,400);
        
        
        add(nom);
        add(nomb);
        add(sex);
        add(Mas);
        add(Fem);
        add(Mas);
        add(Fem);
        add(siso);
        add(mostrar);
        add(pasa);
        add(Musica);
        add(Deporte);
        add(Aventura);
        add(curso);
        add(lista);
        add(txt);
    }

   @Override
    public boolean action (Event e, Object o)
    {
       String datos = "";
       String nomb;
        if(e.target==mostrar)
        {
            nomb=(this.nomb.getText());
            datos += "Nombre: "+nomb+"\n";
            
            if(Mas.getState()==true) {
            datos+= "Sexo: Masculino"+"\n";
            }
            if(Fem.getState()==true) {
            datos+= "Sexo: Femenino"+"\n";
            }
            
            if(siso.getSelectedIndex()==0)
            {
                datos += "Sistema Operativo: Windows"+"\n";
            }
            if(siso.getSelectedIndex()==1)
            {
                datos += "Sistema Operativo: Linux"+"\n";
            }
            
            datos += "Pasatiempo:"+"\n";
            
            if(Musica.getState()==true) {
            datos+= "\tMusica"+"\n";
            }
            if(Deporte.getState()==true) {
            datos+= "\tDeporte"+"\n";
            }
            if(Aventura.getState()==true) {
            datos+= "\tAventura"+"\n";
            }
            
            
            datos += "Curso:"+"\n";
            
            if(lista.getSelectedIndex()==0)
            {
                datos += "\tPHP"+"\n";
            }
            if(lista.getSelectedIndex()==1)
            {
                datos += "\tJAVA"+"\n";
            }
            if(lista.getSelectedIndex()==2)
            {
                datos += "\tASP.NET"+"\n";
            }
            if(lista.getSelectedIndex()==3)
            {
                datos += "\tASP"+"\n";
            }
       
        }
        
        txt.setText(datos);
        return true;
        }
    
   
   
        
        
    

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
